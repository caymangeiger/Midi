# APIs

## Account

- **Method**: `POST`,
- **Path**: `/api/account`

Input:

```json
{
    "username": str,
    "password": str,
}
```

Output:

```json
{
    "username": str,
    "id": int,
}
```

Creating a new account saves the username, password, and id. This adds a new existing Account to the database which can be references with Login and the Authenticator.

## Favorite Artist

- **Method**: `POST`, `GET`, `DELETE`, `GET`
- **Path**: `/api/favorite_artist/create`, `/api/user/favorite_artist`, `/api/favorite_artist/{favorite_artist_id}/delete`, `/api/wikipedia_artist/{search_artist}`

Input:

```json
{
    "name": str,
    "artist_mbid": str,
    "release_group_mbid": str,
    "account_id": int,
}
```

```json
{
    "summary": str
}
```

Output:

```json
{
    "id": int,
    "name": str,
    "artist_mbid": str,
    "release_group_mbid": str,
    "account_id": int,
}
```
```json
{
    "summary": str
}
```

Creating a new favorite artist saves the name, artist mbid, release group mbid, and account id. This adds a new existing favorite artist to the database which can be created and deleted by a user. The wiki get displays the wiki article for the artist.

## Favorite Albums

- **Method**: `POST`, `GET`, `DELETE`, `GET`
- **Path**: `/api/favorite_album/create`, `/api/user/favorite_album`, `/api/favorite_album/{favorite_album_id}/delete`, `/api/wikipedia_album/{search_album_artist}`


Input:

```json
{
    "name": str,
    "artist_name": str,
    "release_group_mbid": str,
    "artist_mbid": str,
    "account_id": int,
}
```

```json
{
    "summary": str
}
```

Output:

```json
{
    "id": int,
    "name": str,
    "artist_name": str,
    "release_group_mbid": str,
    "artist_mbid": str,
    "account_id": int,
}
```
```json
{
    "summary": str
}
```

Creating a new favorite album saves the name, artist name, release group mbid, artist mbid, and account id. This adds a new existing favorite album to the database which can be created and deleted by a user. The wiki get displays the wiki article for the album.


## Review
- **Method**: `POST`, `GET`, `DELETE`, `GET`
- **Path**: `/api/review/create`, `/api/{release_group_mbid}/review`, `/api/review/{review_id}/delete`, `/api/review/{review_id}`


Input:

```json
{
    "rating": condecimal(ge=0.5, le=5.0, decimal_places=1),
    "comment": str,
    "release_group_mbid": str,
    "account_username": str,
}
```


Output:

```json
{
    "id": int,
    "rating": Decimal,
    "comment": str,
    "created_at": datetime,
    "release_group_mbid": str,
    "account_username": str,
}
```

Creating a new review on an album saves the rating, comment, created at date and time, release group mbid, and account username. This adds a new existing review to the database which can be created and deleted by a user to a specific album. The average rating gets an average of all the user ratings for each album
