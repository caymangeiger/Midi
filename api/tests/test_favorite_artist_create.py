# TEST POST AND GET
from authenticator import authenticator
from fastapi.testclient import TestClient
from main import app
from queries.favorite_artist import (
    FavoriteArtistOut,
    FavoriteArtistRepository,
)


client = TestClient(app)


class TestFavoriteArtistRepository:
    def create(
        self,
        favorite_artist,
    ):
        return {
            "id": 1,
            "name": "test",
            "artist_mbid": "123",
            "release_group_mbid": "123",
            "account_id": 1,
        }


def fake_account():
    return {"id": "1", "username": "user"}


def test_fake_create_favorite_artist():
    # Arrange
    app.dependency_overrides[
        authenticator.get_current_account_data
    ] = fake_account

    app.dependency_overrides[
        FavoriteArtistRepository
    ] = TestFavoriteArtistRepository

    fav_artist = {
        "name": "test",
        "artist_mbid": "123",
        "release_group_mbid": "123",
        "account_id": 1,
    }

    # Act
    response = client.post("/api/favorite_artist/create", json=fav_artist)

    # Clean Up
    app.dependency_overrides = {}

    # Assert
    assert response.status_code == 200
    assert response.json() == FavoriteArtistOut(
        id=1,
        name="test",
        artist_mbid="123",
        release_group_mbid="123",
        account_id=1,
    )
