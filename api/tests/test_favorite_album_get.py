from authenticator import authenticator
from main import app
from queries.favorite_album import FavoriteAlbumRepository
from fastapi.testclient import TestClient


client = TestClient(app)


class TestFavoriteAlbumRepository:
    def get_all(
        self,
        account_id
    ):
        return []


def fake_account():
    return {"id": "1", "username": "user"}


def test_get_all_favorite_albums():
    # Arrange
    app.dependency_overrides[
        authenticator.get_current_account_data
    ] = fake_account

    app.dependency_overrides[
        FavoriteAlbumRepository
    ] = TestFavoriteAlbumRepository

    # Act
    response = client.get("/api/user/favorite_album")

    # Clean Up
    app.dependency_overrides = {}

    # Assert
    assert response.status_code == 200
    assert response.json() == []
