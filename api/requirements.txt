fastapi[all]==0.81.0
uvicorn[standard]==0.17.6
pytest==7.1.2
wikipedia==1.4.0
psycopg[binary,pool]==3.1.14
pydantic==1.10.2
jwtdown-fastapi>=0.2.0
