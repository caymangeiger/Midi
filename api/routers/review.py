from fastapi import APIRouter, Depends, Response
from typing import Union, List
from decimal import Decimal
from authenticator import authenticator
from queries.review import (
    Error,
    ReviewIn,
    ReviewOut,
    ReviewRepository,
)


router = APIRouter()


@router.post(
    "/api/review/create",
    response_model=Union[ReviewOut, Error],
)
def create_review(
    review: ReviewIn,
    response: Response,
    repo: ReviewRepository = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    response.status_code = 200
    review.account_username = account_data.get("username")
    return repo.create(review)


@router.delete("/api/review/{review_id}/delete", response_model=bool)
def delete_review(
    review_id: int,
    repo: ReviewRepository = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
) -> bool:
    return repo.delete(review_id)


@router.get(
    "/api/{release_group_mbid}/review",
    response_model=Union[List[ReviewOut], Error],
)
def get_all_reviews(
    release_group_mbid: str,
    repo: ReviewRepository = Depends(),
):
    try:
        return repo.get_all(release_group_mbid)

    except Exception as e:
        print(e)
        return {"message": "Could not get all reviews"}


@router.put(
    "/api/review/{review_id}",
    response_model=Union[ReviewOut, Error],
)
def update_review(
    review_id: int,
    review: ReviewIn,
    repo: ReviewRepository = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
) -> Union[Error, ReviewOut]:
    account_username = account_data.get("username")
    return repo.update(review_id, review, account_username)


@router.get(
    "/api/{release_group_mbid}/average_rating",
    response_model=Decimal,
)
def average_rating(
    release_group_mbid: str,
    repo: ReviewRepository = Depends(),
):
    return repo.average_rating(release_group_mbid)
