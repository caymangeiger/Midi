from pydantic import BaseModel, condecimal
from datetime import datetime
from decimal import Decimal
from typing import List, Union
from queries.pool import pool
from fastapi import (
    HTTPException,
    status,
)


class Error(BaseModel):
    message: str


class ReviewIn(BaseModel):
    rating: condecimal(ge=0.5, le=5.0, decimal_places=1)
    comment: str
    release_group_mbid: str
    account_username: str


class ReviewOut(BaseModel):
    id: int
    rating: Decimal
    comment: str
    created_at: datetime
    release_group_mbid: str
    account_username: str


class ReviewRepository:
    def review_exists(
        self, release_group_mbid: str, account_username: str
    ) -> bool:
        with pool.connection() as conn:
            with conn.cursor() as db:
                db.execute(
                    """
                    SELECT EXISTS (
                        SELECT 1 FROM review
                        WHERE release_group_mbid = %s AND account_username = %s
                    );
                    """,
                    [release_group_mbid, account_username],
                )
                return db.fetchone()[0]

    def create(self, review: ReviewIn) -> ReviewOut:
        if self.review_exists(
            review.release_group_mbid, review.account_username
        ):
            raise HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST,
                detail="User has already commented on this Album",
            )
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        INSERT INTO review
                            (rating, comment, release_group_mbid,
                            account_username)
                        VALUES
                            (%s, %s, %s, %s)
                        RETURNING id, created_at;
                        """,
                        [
                            review.rating,
                            review.comment,
                            review.release_group_mbid,
                            review.account_username,
                        ],
                    )
                    row = result.fetchone()
                    id = row[0]
                    created_at = row[1]
                    return self.review_in_to_out(id, created_at, review)

        except Exception as e:
            print(e)
            return {"message": "Could not create a review"}

    def delete(self, review_id: str) -> bool:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        DELETE FROM review
                        WHERE id = %s
                        """,
                        [review_id],
                    )
                    return True
        except Exception as e:
            print(e)
            return False

    def get_all(
        self, release_group_mbid: str
    ) -> Union[List[ReviewOut], Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT id
                            , rating
                            , comment
                            , created_at
                            , release_group_mbid
                            , account_username
                        FROM review
                        WHERE release_group_mbid = %s
                        """,
                        [release_group_mbid],
                    )
                    result = []
                    for record in db:
                        review = ReviewOut(
                            id=record[0],
                            rating=record[1],
                            comment=record[2],
                            created_at=record[3],
                            release_group_mbid=record[4],
                            account_username=record[5],
                        )
                        result.append(review)
                    return result

        except Exception as e:
            print(e)
            return {"message": "Could not get all reviews"}

    def update(
        self,
        review_id: int,
        review: ReviewIn,
        account_username: int,
    ) -> Union[ReviewOut, Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        UPDATE review
                        SET rating = %s
                            , comment = %s
                        WHERE id = %s AND account_username = %s
                        """,
                        [
                            review.rating,
                            review.comment,
                            review_id,
                            account_username,
                        ],
                    )
                    db.execute(
                        """
                        SELECT id
                            , rating
                            , comment
                            , created_at
                            , release_group_mbid
                            , account_username
                        FROM review
                        WHERE id = %s
                        """,
                        [review_id],
                    )
                    row = db.fetchone()
                    id = row[0]
                    created_at = row[4]
                old_data = review.dict()
                return ReviewOut(id=id, created_at=created_at, **old_data)

        except Exception as e:
            print(e)
            return {"message": "Could not update review"}

    def average_rating(
        self,
        release_group_mbid: str,
    ) -> Union[Decimal, Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    (
                        db.execute(
                            """
                        SELECT ROUND(AVG(rating), 1) AS average_rating
                        FROM review
                        WHERE release_group_mbid = %s
                        """,
                            [release_group_mbid],
                        ),
                    )
                    row = db.fetchone()
                    average_rating = (
                        row[0] if row[0] is not None else Decimal(0.0)
                    )
                    return average_rating

        except Exception as e:
            print(e)
            return {"message": "Could not get all reviews"}

    def review_in_to_out(
        self, id: int, created_at: datetime, review: ReviewIn
    ):
        old_data = review.dict()
        return ReviewOut(id=id, created_at=created_at, **old_data)
