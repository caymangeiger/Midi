import os
from psycopg_pool import ConnectionPool
from dotenv import load_dotenv

load_dotenv()


database_url = os.getenv('DATABASE_URL')


pool = ConnectionPool(conninfo=database_url)
