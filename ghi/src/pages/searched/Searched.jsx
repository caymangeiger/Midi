import "./Searched.css";
import { useState, useEffect, useCallback } from "react";
import { useToast } from "../../context/toastcontext/useToast";
import { useData } from "../../context/datacontext/useData";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPlus } from '@fortawesome/free-solid-svg-icons';
import { useAuthContext } from "../../authorization/Authorization";
import { useNavigate } from "react-router-dom";
import { useAccountLoginModalContext } from "../../modals/accounts/login/useAccountLoginModalContext";
import useWindowSize from "../../extrafunctions/helpers/WindowSize";


export default function Searched() {
    const [isLoading, setIsLoading] = useState(false);
    const [searchIsBuffering, setSearchIsBuffering] = useState(false);
    const [scrollIsBuffering, setScrollIsBuffering] = useState(false);
    const [offset, setOffset] = useState(0);
    const [artists, setArtists] = useState([]);
    const { setArtistName, setArtistMbid } = useData();
    const { searchInput } = useData();
    const showToast = useToast();
    const { token } = useAuthContext();
    const navigate = useNavigate();
    const { openAccountLoginModal } = useAccountLoginModalContext();
    const { width } = useWindowSize();
    const limitPerWidth = width < 600 ? 6 : (width < 900 ? 6 : 8);
    const defaultImage =
        "https://st3.depositphotos.com/23594922/31822/v/450/depositphotos_318221368-stock-illustration-missing-picture-page-for-website.jpg";
    const loadingImage = "https://i.giphy.com/media/daak2Jqk5NZN2G4PKD/giphy.webp"


    useEffect(() => {
        setOffset(0);
        setArtists([]);
        setIsLoading(true);
    }, [searchInput]);


    const fetchWithRetry = useCallback(async (url, options, retries = 3, delayDuration = 1000) => {
        await delay(100)
        try {
            const response = await fetch(url, options);
            if (!response.ok) {
                throw new Error(`Request failed with status ${response.status}`);
            }
            return await response.json();
        } catch (error) {
            if (retries > 0) {
                await delay(delayDuration);
                return await fetchWithRetry(url, options, retries - 1, delayDuration);
            } else {
                throw new Error(`Max retries reached for ${url}`);
            }
        }
    }, []);


    const delay = (ms) => new Promise(resolve => setTimeout(resolve, ms));
    const loadSearched = useCallback(async () => {
        try {
            const response = await fetch(
                `https://musicbrainz.org/ws/2/artist?query=artist:"${searchInput}"&fmt=json&limit=${limitPerWidth}&offset=${offset}`
            );
            if (response.ok) {
                const data = await response.json();
                const newArtists = data.artists.map(artist => ({ ...artist, albums: [], loading: true }));
                const existingArtistIds = new Set(artists.map(artist => artist.id));
                const uniqueNewArtists = newArtists.filter(artist => !existingArtistIds.has(artist.id));
                const updatedArtists = [...artists, ...uniqueNewArtists];
                setArtists(updatedArtists);

                for (const [index, artist] of uniqueNewArtists.entries()) {
                    setScrollIsBuffering(true);
                    let albumsOnly = [];
                    if ((index + 1) % 4 === 0 || artist.albums.id) {
                        await delay(200);
                    } else {
                        await delay(200);
                    }
                    try {
                        const albumResponse = await fetchWithRetry(
                            `https://musicbrainz.org/ws/2/release-group?artist=${artist.id}&fmt=json&primarytype=album&sort=first-release-date&limit=4`,
                        );
                        albumsOnly = albumResponse["release-groups"].filter(
                            (item) => item["primary-type"] === "Album"
                        );
                    } catch (error) {
                        console.error("Error fetching album data:", error);
                    }

                    setArtists(currentArtists => currentArtists.map(
                        currentArtist => currentArtist.id === artist.id
                            ? { ...currentArtist, albums: albumsOnly, loading: false }
                            : currentArtist
                    ));
                }
                await delay(400);
                setScrollIsBuffering(false);
            }
        } catch (error) {
            console.error("Error fetching search input", error);
        }
        setIsLoading(false);
    }, [searchInput, limitPerWidth, offset, artists, fetchWithRetry]);


    useEffect(() => {
        if (isLoading && (offset || artists.length === 0)) {
            loadSearched();
        }
    }, [isLoading, offset, artists.length, loadSearched]);


    useEffect(() => {
        function startTimer() {
            let counter = 500
            const intervalId = setInterval(() => {
                counter--;
                if (counter === 0) {
                    clearInterval(intervalId);
                    setSearchIsBuffering(false)
                    counter = 500
                }
            }, 1);
        }
        startTimer()
    }, []);



    const handleAddClick = async (name, mbid, releaseMbid) => {
        if (!token) {
            showToast("Please Login!", "error")
            openAccountLoginModal()
        } else {
            if (releaseMbid) {
                try {
                    const response = await fetch(
                        `${import.meta.env.VITE_APP_API_HOST}/api/favorite_artist/create`,
                        {
                            method: "POST",
                            credentials: "include",
                            body: JSON.stringify({
                                name: `${name}`,
                                artist_mbid: `${mbid}`,
                                release_group_mbid: `${releaseMbid}`,
                                account_id: 0,
                            }),
                            headers: {
                                "Content-Type": "application/json",
                                Authorization: `Bearer ${token}`,
                            },
                        }
                    );
                    if (response.ok) {
                        showToast("Added To Favorites!", "success");
                    } else {
                        showToast("Could Not Add To Favorites!", "error");
                    }
                } catch (error) {
                    console.error("Error adding to favorites:", "error");
                    showToast("You've Already Favorited This Artist!", "error");
                }
            }
        }
    };


    function artistClick(e) {
        const artistDiv = e.currentTarget.closest("#artistName");
        const name = artistDiv.getAttribute("data-name");
        const artistMbid = artistDiv.getAttribute("data-mbid");
        if (artistMbid && name && searchIsBuffering === false) {
            setArtistName(name);
            setArtistMbid(artistMbid);
            navigate("/artist")
        } else {
            showToast("Please Wait!", "error")
        }
    }




    const searchedImageDivs = document.querySelectorAll('.searchedImageDiv');

    searchedImageDivs.forEach(div => {
        div.addEventListener('mouseenter', () => {
            searchedImageDivs.forEach(otherDiv => otherDiv.classList.remove('active'));
            div.classList.add('active');
        });
    });

    return (
        <div className="mainSearchedDiv">
            <div className="searchedForDiv">
                <h1 className="searchedFor">Search Results For: {searchInput}</h1>
            </div>
            <div
                className={
                    artists.length === 0
                        ? "mainSearchedDivChildNone"
                        : "mainSearchedDivChild"
                }
            >
                {artists.length === 0 && <h1> No Results Found </h1>}
                {artists.length >= 1 &&
                    artists.map((artist) => {
                        return (
                            <div
                                key={artist.id}
                                data-name={artist.name}
                                data-mbid={artist.id}
                                id="artistName"
                                className="searchedGridsDiv"
                            >
                                <div className="FavoritesGridsDivChild">
                                    <div className="FavoritesNameAndButtonDiv">
                                        <div className="FavoritesNameDiv">
                                            <button
                                                onClick={artistClick}
                                                className="searchedName"
                                            >
                                                {artist.name}
                                            </button>
                                        </div>
                                        <div className="FavoritesButtonDiv">
                                            <FontAwesomeIcon
                                                icon={faPlus}
                                                onClick={() =>
                                                    handleAddClick(
                                                        artist.name,
                                                        artist.id,
                                                        artist.albums[0].id
                                                    )
                                                }
                                                className="searchedButton"
                                            />
                                        </div>
                                    </div>
                                    <div className="searchedImageDiv">
                                        <div className="overlayPhoto"></div>
                                        {artist.loading ? (
                                            <img className="searchedImage" src={loadingImage} />
                                        ) : artist.albums.length > 0 ? (
                                            <div className={artist.albums[0].id && artist.albums.length > 0 ? "photoSearchDiv" : ""}>
                                                <img
                                                    key={artist.albums[0].id}
                                                    className="searchedImage"
                                                    src={`https://coverartarchive.org/release-group/${artist.albums[0].id}/front`}
                                                    alt={`${artist.name}`}
                                                    onError={(e) => e.target.src = defaultImage}
                                                    onClick={artistClick}
                                                />
                                                <img src="/cdArt1.png" alt="CD" className="cd" />
                                            </div>
                                        ) : (
                                            <div className="noImageDiv">
                                                <img className="searchedNoImage" src={defaultImage} alt="Default image" />
                                            </div>
                                        )}
                                    </div>
                                </div>
                            </div>
                        );
                    })}
            </div>
            {artists.length >= 6 && (
                scrollIsBuffering ?
                    <div style={{ display: "flex", justifyContent: "center", height: "fit-content", overflow: "hidden" }}>
                        <img
                            style={{ width: "200px", height: "200px", padding: "0", marginTop: "-10px" }}
                            src="https://media.giphy.com/media/gLUPZW8YG7QIhK17lA/giphy.gif"
                        />
                    </div>
                    :
                    <div style={{ display: "flex", justifyContent: "center" }}>
                        <button
                            className="loadMoreButton"
                            onClick={() => {
                                setIsLoading(true);
                                setOffset(prevOffset => prevOffset + 9);
                            }}>
                            Load More
                        </button>
                    </div>
            )}
        </div>
    );
}
