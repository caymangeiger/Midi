import "bootstrap/dist/css/bootstrap.min.css";
import { useState, useEffect } from "react";
import "./MainPage.css";
import { useAccountCreateModalContext } from "../../modals/accounts/create/useAccountCreateModalContext";
import { useAuthContext } from "../../authorization/Authorization";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
    faMagnifyingGlass,
} from "@fortawesome/free-solid-svg-icons";
import { useNavigate } from "react-router-dom";
import { useData } from "../../context/datacontext/useData";


function MainPage() {
    const [currentIndex, setCurrentIndex] = useState(0);
    const transitionTime = 2500;
    const { openAccountCreateModal } = useAccountCreateModalContext();
    const [searchInputChange, setSearchInputChange] = useState("");
    const { setSearchInput, setReleaseMbid, setArtistMbid, setArtistName, setReleaseGroupMbid, setAlbumName } = useData();
    const { token } = useAuthContext();
    const navigate = useNavigate();


    const handleInputChange = (e) => {
        setSearchInputChange(e.target.value);
    };

    const handleAction = () => {
        setSearchInput(searchInputChange)
        navigate("/search");
        setSearchInputChange("");
    };

    const handleKeyDown = (e) => {
        if (e.key === "Enter") {
            handleAction();
        }
    };


    useEffect(() => {
        setReleaseMbid("")
        setReleaseGroupMbid("")
        setAlbumName("")
        setArtistName("")
        setArtistMbid("")
        const timer = setInterval(() => {
            setCurrentIndex((prevIndex) => (prevIndex + 1) % 3);
        }, transitionTime);

        return () => clearInterval(timer);
    }, [setAlbumName, setArtistMbid, setArtistName, setReleaseGroupMbid,
        setReleaseMbid]);

    const getTransformStyle = (index) => {
        return {
            transform: `translateX(-${index * 100}%)`,
            width: `${3 * 20}%`,
        };
    };

    return (
        <div className="mainPageDiv">
            <div className="mainPageDivChild">
                <div className="mainPageContentDiv">
                    <h1 className="mainPagePhotoTitle">One Stop For Your Music Needs!</h1>
                    <div className="carousel">
                        <div
                            className="carousel-images"
                            style={getTransformStyle(currentIndex)}
                        >
                            <img
                                className="carousel-image"
                                src="https://images.theconversation.com/files/512871/original/file-20230301-26-ryosag.jpg?ixlib=rb-1.1.0&rect=97%2C79%2C5799%2C5817&q=45&auto=format&w=926&fit=clip"
                                alt="First Image"
                            />
                            <img
                                className="carousel-image"
                                src="https://wknc.org/wp-content/uploads/2021/04/Screen-Shot-2021-04-08-at-5.00.44-PM.png"
                                alt="Second Image"
                            />
                            <img
                                className="carousel-image"
                                src="https://www.rockarchive.com/media/2079/pink-floyd-pflo022st.jpg?width=800&height=803&mode=stretch&overlay=watermark.png&overlay.size=230,20&overlay.position=0,783"
                                alt="Third Image"
                            />
                        </div>
                    </div>
                </div>
                <div className="mainPageSignUpContentDiv">
                    {!token && (
                        <button onClick={openAccountCreateModal} className="mainPageSignUpButton">
                            Sign Up Today
                        </button>
                    )}
                </div>
                <div>
                    {token && (
                        <div className="searchInputHomeDivParent">
                            <div className="searchInputHomeDiv">
                                <input
                                    className="searchInputHome"
                                    placeholder="Search Artists"
                                    type="text"
                                    value={searchInputChange}
                                    onChange={handleInputChange}
                                    onKeyDown={handleKeyDown}
                                />
                                <FontAwesomeIcon
                                    icon={faMagnifyingGlass}
                                    onClick={handleAction}
                                    className="searchIconHome"
                                />
                            </div>
                        </div>
                    )}
                </div>
            </div>
        </div>
    );
}
export default MainPage;
