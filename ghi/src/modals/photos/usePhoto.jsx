import { useContext } from 'react';
import { PhotoModalContext } from "./PhotoContext"


export const usePhotoModal = () => {
    return useContext(PhotoModalContext);
}
