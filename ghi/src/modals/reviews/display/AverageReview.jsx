import { useState, useEffect } from 'react';
import PropTypes from "prop-types";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faStar as farFaStar } from '@fortawesome/free-regular-svg-icons';
import { faStar as fasFaStar } from '@fortawesome/free-solid-svg-icons';
import { faStarHalfAlt } from '@fortawesome/free-solid-svg-icons';
import "../create/ReviewingForm.css";


export default function AverageReview({  starStyle = {}, releaseGroupMbid, newReviewSubmitted }) {
    const [albumRating, setAlbumRating] = useState(null);


    AverageReview.propTypes = {
    starStyle: PropTypes.object,
    releaseGroupMbid: PropTypes.string.isRequired,
    newReviewSubmitted: PropTypes.bool.isRequired,
    };


    useEffect(() => {
        async function loadRating() {
            try {
                const response = await fetch(`${import.meta.env.VITE_APP_API_HOST}/api/${releaseGroupMbid}/average_rating`, {
                    credentials: 'include',
                    headers: {
                        "Content-Type": "application/json",
                    },
                }
                )

                if (!response.ok) {
                    throw new Error('Response was not ok');
                }
                const data = await response.json();
                if (data) {
                    setAlbumRating(data);
                }
            } catch (error) {
                console.error('Cannot get the rating:', error);
            }
        }
        loadRating();
    }, [newReviewSubmitted, releaseGroupMbid]);


    return (
    <div className="review_stars">
        {albumRating && (
            <div>
                {
                    [1, 2, 3, 4, 5].map((starNumber) => {
                        let iconToUse = farFaStar;
                        const currentRating = albumRating;
                        if (currentRating >= starNumber) {
                            iconToUse = fasFaStar;
                        } else if (currentRating >= starNumber - 0.5) {
                            iconToUse = faStarHalfAlt;
                        }
                        return (
                            <FontAwesomeIcon
                                key={starNumber}
                                className="starIcon fa-fw"
                                icon={iconToUse}
                                color="transparent"
                                style={{
                                    color: iconToUse !== farFaStar ? 'gold' : 'white',
                                    ...starStyle
                                }}
                            />
                        );
                    })
                }
            </div>
        )}
        {!albumRating && (
            <h6>
                No Reviews Yet!
            </h6>
        )}
        </div>
    );
}
