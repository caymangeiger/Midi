My Journal

November

--------------------------------------------------------------------
6th~

-I presented the Idea of making a digital CD booklet using a "blueprint" from the MusicBrainz website

-Cayman created a Canva for us to see a rough sketch of a front end design

--------------------------------------------------------------------
7th~

-Cayman created a Notion with all the APIs and a todo list

-We tried dealing with the Wiki pages seeing how to parce through all the data from just one page.
after about almost 2 hours of messing with different APIs for possibly getting upcoming concert information

-Bart showed up  and showed us commands in the terminal that actually parces the data by sections, it really helped us out.

-Riley created a Wire Frame in Excaludraw

-we went over the Wire Frames

--------------------------------------------------------------------
8th~

-Began a DEV-MIDI to test out the APIs

--------------------------------------------------------------------
9th~

-Went over the Wire Frame again

--------------------------------------------------------------------
13th~

-Created the Repo for the Project
-Converted to Vite

--------------------------------------------------------------------

15th~

-Began to work on getting a tracklist to populate

-worked with the Artist Api, the Album Apis to get the Tracklist

--------------------------------------------------------------------

16th~

-Cayman helped me figure out some issues i was having with pulling the data for the Tracklist

--------------------------------------------------------------------

17th~

-Me and Cayman got the Tracklist to a good spot where we can finish the favorite page and implement them both to one page

---------------------------------------------------------------------

20th~

-Worked with Riley on connecting the front-end to fast API.
-ran into Docker issues that lasted majority of the day
-Got Docker resolved and began working on searching for an artist with Cayman and Riley

---------------------------------------------------------------------

21st~

-Me and Cayman fixed bug in the API calls dealing with a limit. MusicBrainz Api defaults to return 25 objects, we manually set a -limit to 100 and so on if there is more.

---------------------------------------------------------------------

22nd~

-Began working on the Sign up, Sign in modal to redirect a user if they already have an account

-Helped Cayman with parsing Wiki-page info for Artist

----------------------------------------------------------------------

27th~

-Worked on building a mp3 player using a spotify api where users can look up songs and listen to them with their spotify account
have it working but am not sure how the accounts work with users. Not sure if my account is ultimately tied to this and people use my account or if they can use their own account

-----------------------------------------------------------------------

28th~

-Worked on fixing a delete onClick for favorite artist and Albums

-----------------------------------------------------------------------

29th~

-Finished the delete functionality for the Favorite Artists and Albums

-----------------------------------------------------------------------

30th~

-Worked on populating the photos for each album.
-realized that the albums we are retreiving do not have the most art work like we want

-----------------------------------------------------------------------

4th~

-Worked on making Favorite Albums clickable links to said album detail page

-------------------------------------------------------------------------

5th~

-Worked with Cayman to finish the filter for Albums with highest count of cover art and highest count of track titles

-------------------------------------------------------------------------
