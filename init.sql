DROP TABLE IF EXISTS account;
DROP TABLE IF EXISTS favorite_artist;
DROP TABLE IF EXISTS favorite_album;
DROP TABLE IF EXISTS review;

CREATE TABLE IF NOT EXISTS account (
    id SERIAL PRIMARY KEY,
    username VARCHAR(150) UNIQUE NOT NULL,
    hashed_password VARCHAR(150) NOT NULL
);

CREATE TABLE IF NOT EXISTS favorite_artist (
    id SERIAL PRIMARY KEY,
    name VARCHAR(1000) NOT NULL,
    artist_mbid VARCHAR(100) NOT NULL,
    release_group_mbid VARCHAR(100) NOT NULL,
    account_id INTEGER REFERENCES account(id) ON DELETE CASCADE NOT NULL
);

CREATE TABLE IF NOT EXISTS favorite_album (
    id SERIAL PRIMARY KEY,
    name VARCHAR(1000) NOT NULL,
    artist_name VARCHAR(1000) NOT NULL,
    release_group_mbid VARCHAR(100) NOT NULL,
    artist_mbid VARCHAR(100) NOT NULL,
    account_id INTEGER REFERENCES account(id) ON DELETE CASCADE NOT NULL
);

CREATE TABLE IF NOT EXISTS review (
    id SERIAL PRIMARY KEY,
    rating DECIMAL(3, 1) CHECK (rating >= 0.5 AND rating <= 5.0) NOT NULL,
    comment TEXT,
    created_at TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
	release_group_mbid VARCHAR(100) NOT NULL,
    account_username VARCHAR REFERENCES account(username) ON DELETE CASCADE NOT NULL
);
